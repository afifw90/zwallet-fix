//
//  AmountInteractor.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import Foundation


protocol AmountInteractor {
    func postTransaction(receiver: Int, amount: Int, notes: String)
    func getBalance()
}
