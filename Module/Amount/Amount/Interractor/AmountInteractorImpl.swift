//
//  AmountInteractorImpl.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core


class AmountInteractorImpl: AmountInteractor {
    
    
    
    var interactorOutput: AmountInteractorOutput?
    var contacNetworkManager: ContacNetworkManager
    var balaceNetWorkManager: BalanceNetworkManager
    
    
    init(networkManager: ContacNetworkManager, balancenetWorkManager: BalanceNetworkManager) {
        self.contacNetworkManager = networkManager
        self.balaceNetWorkManager = balancenetWorkManager
    }
    
    func postTransaction(receiver: Int, amount: Int, notes: String) {
        
        self.contacNetworkManager.createNewTransfer(receiver: receiver, amount: amount, notes: notes) { data, error in
            
            if let transferData = data {
                self.interactorOutput?.authenticationRersoult(isSuccess: true)
            } else {
                self.interactorOutput?.authenticationRersoult(isSuccess: false)
            }
            
            
        }
    }
    
    func getBalance() {
        self.balaceNetWorkManager.getBalance { (data, error) in
            if let balance = data {
                let amount = BalanceAmount(balance: balance.balance)
                self.interactorOutput?.getBalance(balance: amount)
            }
        }
    }
}
