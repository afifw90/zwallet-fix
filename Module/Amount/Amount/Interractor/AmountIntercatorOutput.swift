//
//  AmountIntercatorOutput.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

protocol AmountInteractorOutput {
    func authenticationRersoult(isSuccess: Bool)
    func getBalance(balance: BalanceAmount)
}

