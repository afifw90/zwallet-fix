//
//  AmountPresenter.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit

protocol AmountPresenter {
    func transfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String)
    
    
    func backtoConfirmTransfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String)
    
    func navigateToContac()
    
    func getBalance()
}
