//
//  AmountPresenterImpl.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

class AmountPresenterImpl: AmountPresenter {
    
    
    
    
    let view: AmountView
    let interactor: AmountInteractor
    let router: AmountRouter
    
    init (view: AmountView, interactor: AmountInteractor, router: AmountRouter){
        
        self.view = view
        self.interactor = interactor
        self.router = router
        
    }
    
    func transfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String) {
        router.nafigateToTransverPinChek(viewController: viewController, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes)
        
    }
    
    func backtoConfirmTransfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String) {
    
    }
    
    func navigateToContac() {
        router.navigateToContac()
    }
    
    func getBalance() {
        self.interactor.getBalance()
    }
    
    
}


extension AmountPresenterImpl: AmountInteractorOutput {
    func getBalance(balance: BalanceAmount) {
        self.view.getBalance(balance: balance)
    }
    
    func authenticationRersoult(isSuccess: Bool) {
        if isSuccess {
//            self.router.nafigateToTransverPinChek(viewController: <#UIViewController#>, id: <#Int#>, name: <#String#>, phone: <#String#>, image: <#String#>, amount: <#Int#>, notes: <#String#>)
        } else {
            self.view.showError()
        }
    }
    
}
