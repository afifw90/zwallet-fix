//
//  AmountRouter.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit

protocol AmountRouter {
    func nafigateToTransverPinChek(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String)
    func navigateToContac()
}
