//
//  AmountRouterImpl.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core


public class AmountRouterImpl {
    
    
    public static func navigateToModule(viewController: UIViewController,id: Int, name: String, phone: String, image: String) {
        let bundle = Bundle(identifier: "com.mandiri.Amount")
        let vc = AmountViewController(nibName: "AmountViewController", bundle: bundle)
        
        
        
        let networkManager = ContacNetworkManagerImpl()
        let balamncenetworkmanager = BalanceNetworkManagerImpl()
        
        let router = AmountRouterImpl()
        let interactor = AmountInteractorImpl(networkManager: networkManager, balancenetWorkManager: balamncenetworkmanager)
        let presenter = AmountPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        vc.id = id
        vc.name = name
        vc.phone = phone
        vc.image = image
        
        
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    
}

extension AmountRouterImpl: AmountRouter {
    func nafigateToTransverPinChek(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String) {
        AppRouter.shared.navigateToTransferConfirm(viewController: viewController, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes)
        
    }
    
    func navigateToContac() {
        AppRouter.shared.navigateToContac()
    }
    
}
