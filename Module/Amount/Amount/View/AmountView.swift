//
//  AmountView.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

protocol AmountView {
    func showError()
    func getBalance(balance: BalanceAmount)
}
