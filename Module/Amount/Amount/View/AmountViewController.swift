//
//  AmountViewController.swift
//  Amount
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Core
import Kingfisher


class AmountViewController: UIViewController {

    
    @IBOutlet weak var amountText: UITextField!
    @IBOutlet weak var notesText: UITextField!
    
    
    @IBOutlet weak var viewLabel: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneText: UILabel!
    
    @IBOutlet weak var balanceText: UILabel!
    
    
    var presenter: AmountPresenter?
    
    
    var id: Int = 0
    var name: String = ""
    var phone: String = ""
    var image: String = ""
    var balanceNow: Int = 0
    var balanceFormat: String = ""
    
    @IBAction func gotoChekPin(_ sender: Any) {
        let amount: Int = Int(amountText.text ?? "")!
        let notes: String = notesText.text ?? ""
        
        if amount > balanceNow {
            self.showError()
        }
        else{
            self.presenter?.transfer(viewController: self, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes)
        }
       
        
        
    }
    
    
    @IBAction func backToGetContac(_ sender: Any) {
        self.presenter?.navigateToContac()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = name
        phoneText.text = phone
        viewLabel.kf.setImage(with: URL(string: image))
        self.presenter?.getBalance()
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AmountViewController: AmountView {
    
    func getBalance(balance: BalanceAmount) {
        balanceNow = balance.balance
        balanceFormat = balance.balance.formatToIdr()
        balanceText.text = "\(balanceFormat) Available"
    }
    
    
    func showError() {
        
            let alert = UIAlertController (title: "Peringatan", message: "Saldo anda tidak cukup", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        
    }
    
}



