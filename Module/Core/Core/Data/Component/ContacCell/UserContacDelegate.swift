//
//  UserContacDelegate.swift
//  Core
//
//  Created by MacBook on 28/05/21.
//

import Foundation

public protocol UserContacDelegate {
    func showTransfer(id: Int, name: String, phone: String, image: String)
}
