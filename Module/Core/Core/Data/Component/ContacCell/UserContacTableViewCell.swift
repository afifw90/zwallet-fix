//
//  UserContacTableViewCell.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import UIKit

public class UserContacTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var phoneText: UILabel!
    @IBOutlet weak var contacImage: UIImageView!
    
    @IBOutlet weak var viewContact: UIView!
    
    public var delegate: UserContacDelegate?
    
    var id: Int?
    var url: String?
    
    public func showData(contacData: UserContacEntity){
        self.nameText.text = contacData.name
        self.phoneText.text = contacData.phone
        
        self.id = contacData.id
        self.url = contacData.image
        
        let url = URL(string: contacData.image)
        self.contacImage.kf.setImage(with: url)
        
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewContact.addGestureRecognizer(tap)
    }

    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.delegate?.showTransfer(id: self.id ?? 0, name: self.nameText.text ?? "", phone: self.phoneText.text ?? "", image: self.url ?? "")
    }
}
