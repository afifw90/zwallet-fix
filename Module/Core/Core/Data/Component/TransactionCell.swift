//
//  TransactionCell.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Kingfisher

public class TransactionCell: UITableViewCell {

    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func showData(transection: TransactionEntity){
        self.nameLabel.text = transection.name
        self.noteLabel.text = transection.notes
        
        
        if transection.type == "in"{
            self.amountLabel.text = "+\(transection.amount.formatToIdr())"
            self.amountLabel.textColor = .green
        }else {
            self.amountLabel.text = "-\(transection.amount.formatToIdr())"
            self.amountLabel.textColor = .red
        }
        print("Ini data")
        let url = URL(string: transection.imageUrl)
        self.userImage.kf.setImage(with: url)
    }
    
    
    
    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
