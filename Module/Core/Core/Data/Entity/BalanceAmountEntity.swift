//
//  BalanceAmountEntity.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation

public struct BalanceAmount {
    
    public var balance: Int
    
    
    public init(balance: Int){
        
        self.balance = balance
    }
    
    
}
