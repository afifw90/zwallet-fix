//
//  UserContacEntity.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
public struct UserContacEntity {
    
    var id: Int
    var name: String
    var phone: String
    var image: String
    
    
    
    public init(name: String, phone: String, image: String, id: Int){
        self.name = name
        self.phone = phone
        self.image = image
        self.id = id
    }
    
}
