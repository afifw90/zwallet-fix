//
//  AuthApi.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import Moya

public enum AuthApi {
    case login(email: String, password: String)
    case signUp(username: String, email: String, password: String)
    case setPin(pin: String)
    case setOtp(email: String, otp: String)
    
    case refreshToken(email: String, refreshToken: String)
}

extension AuthApi: TargetType {
    public var path: String{
        switch self {
        case .login:
            return "/auth/login"
        case .signUp:
            return "/auth/signup"
        case .setPin:
            return "/auth/PIN"
        case .setOtp(let email, let otp):
            return "/auth/activate/\(email)/\(otp)"
        case .refreshToken:
            return "/auth/refresh-token"
            
        }
        
     
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self{
        case .login(let email, let password):
            return .requestParameters(
                parameters: ["email": email, "password": password],
                encoding: JSONEncoding.default
            )
        case .signUp(let username,let email,let password):
            return .requestParameters(parameters: ["username": username, "email": email, "password": password], encoding: JSONEncoding.default)
        case .setPin(let pin):
            return .requestParameters(parameters: ["PIN": pin], encoding: JSONEncoding.default)
        case .setOtp:
            return .requestPlain
        case .refreshToken(let email, let refreshToken):
        return .requestParameters(parameters: ["email": email, "refreshToken": refreshToken], encoding: JSONEncoding.default)
            
            
        }
    }
    
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .signUp:
            return .post
        case .setPin:
            return .patch
        case .setOtp:
            return .get
        case .refreshToken:
            return .post
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        case .setPin:
            let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
            return [
                "Content-Type" : "application/json",
                "Authorization" : "Bearer \(token)"
            ]
        case .setOtp:
            let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
            return [
                "Content-Type" : "application/json",
                "Authorization" : "Bearer \(token)"
            ]
        case .refreshToken:
            let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
            return [
                "Content-Type" : "application/json",
                "Authorization" : "Bearer \(token)"
            ]
            
        default:
            return [
                "Content-Type": "application/json"
            ]
        }
        
    }
}
