//
//  RefreshTokenResponse.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation
struct RefreshTokenResponse: Codable {
    var status: Int
    var message: String
    var data: RefreshTokenDataResponse
}
