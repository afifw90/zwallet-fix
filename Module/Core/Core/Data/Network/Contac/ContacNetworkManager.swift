//
//  ContacNetworkManager.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation

public protocol ContacNetworkManager {
    func getAllContac(completion: @escaping ([GetContacDataResponse]?, Error?) -> ())
    func createNewTransfer(receiver: Int, amount: Int, notes: String, completion: @escaping (TransferResponse?, Error?) -> ())
}
