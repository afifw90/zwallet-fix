//
//  ContacNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public class ContacNetworkManagerImpl: ContacNetworkManager {
    
    
    
    public init(){
        
    }
    
    public func getAllContac(completion: @escaping ([GetContacDataResponse]?, Error?) -> ()) {
        let provider = MoyaProvider<ContacApi>(plugins:[NetworkLoggerPlugin(configuration: .init(logOptions:.verbose))], isRefreshToken: true)
        provider.request(.getAllContac) { response in
            switch response{
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getInvoiceResponse = try decoder.decode(GetContacResponse.self, from: result.data)
                    completion(getInvoiceResponse.data, nil)
                } catch let error {
                    completion (nil, error)
                }
            
            case .failure(let error):
                completion(nil, error)
            }
            
        }
    }
    
    public func createNewTransfer(receiver: Int, amount: Int, notes: String, completion: @escaping (TransferResponse?, Error?) -> ()) {
        let provider = MoyaProvider<TransferApi>(plugins:[NetworkLoggerPlugin(configuration: .init(logOptions:.verbose))], isRefreshToken: true)
        provider.request(.createNewTransfer(receiver: receiver, amount: amount, notes: notes)) { response in
            switch response{
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getTransferResponse = try decoder.decode(TransferResponse.self, from: result.data)
                    completion(getTransferResponse, nil)
                } catch let error {
                    completion (nil, error)
                }
            
            case .failure(let error):
                completion(nil, error)
            }
            
        }
    }
    
}
