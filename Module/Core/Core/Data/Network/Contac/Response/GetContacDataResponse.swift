//
//  GetContacDataResponse.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation

public struct GetContacDataResponse: Codable {
    
    public var id: Int
    public var name: String
    public var phone: String
    public var image: String
    
}
