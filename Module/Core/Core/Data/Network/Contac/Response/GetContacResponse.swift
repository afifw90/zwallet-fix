//
//  GetContacResponse.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation


public struct GetContacResponse: Codable {
    
    var status: Int
    var data: [GetContacDataResponse]
    
}
