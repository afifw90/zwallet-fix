//
//  TransferDataResponse.swift
//  Core
//
//  Created by MacBook on 28/05/21.
//

import Foundation

public struct TransferDataResponse: Codable {
    
    public var sender: Int
    public var receiver: Int
    public var amount: Int
    public var notes: String
    public var type: Int
    public var created_at: String
    
}
