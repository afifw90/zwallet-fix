//
//  TransferApi.swift
//  Core
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Moya


public enum TransferApi {
    case createNewTransfer(receiver: Int, amount: Int, notes: String)
}

extension TransferApi: TargetType {
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var path: String {
        switch self {
        case .createNewTransfer:
            return "/tranfer/newTranfer"
        }
    }
    
    public var method: Moya.Method {
        return .post
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .createNewTransfer(let receiver, let amount, let notes):
            return .requestParameters(parameters: ["receiver": receiver, "amount": amount, "notes": notes], encoding: JSONEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        let pin: String = UserDefaultHelper.shared.get(pin: .userPin) ?? ""
        return [
            
            "Authorization": "Bearer \(token)",
            "x-access-PIN": pin
        ]
    }
    
    
    
    
}
