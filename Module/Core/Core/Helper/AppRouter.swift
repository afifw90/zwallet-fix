//
//  AppRouter.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import UIKit

public class AppRouter {
    
    public static let shared: AppRouter = AppRouter()
    
    public var loginScene: (() -> ())? = nil
    
    public func navigateToLogin() {
        
        self.loginScene?()
        
    }
    
    public var homeScene: (() -> ())? = nil
    
    public func navigateToHome(){
        self.homeScene?()
    }
    
//    public var hisrtoryScene: (( _ viewController: UIViewController) -> ())? = nil
//
//    public func navigateToHistory(_ viewController: UIViewController) {
//        self.hisrtoryScene?(viewController)
//    }
    
    public var hisrtoryScene: (() -> ())? = nil
    
    public func navigateToHistory() {
        self.hisrtoryScene?()
    }
    
    public var signUpScene: (() -> ())? = nil
    
    public func navigateToRegister() {
        self.signUpScene?()
    }
    
    public var createPin: (() -> ())? = nil
    
    public func navigateToCreatePin() {
        self.createPin?()
    }
    
    public var setOtp: (() -> ())? = nil
    
    public func navigateToSetOtp() {
        self.setOtp?()
    }
    
    public var getContac: (() -> ())? = nil
    
    public func navigateToContac() {
        self.getContac?()
    }
    
    public var goTransfer: ((_ viewController: UIViewController,_ id: Int, _ name: String, _ phone: String, _ image: String) -> ())? = nil
    
    public func navigateToTransfer(_ viewController: UIViewController,id: Int, name: String, phone: String, image: String) {
        self.goTransfer?(viewController,id, name, phone, image)
    }
    
    public var gotoConfirmTrasver: ((_ viewController: UIViewController, _ id: Int, _ name: String, _ phone: String, _ image: String, _ amount: Int, _ notes: String) -> ())? = nil
    
    public func navigateToTransferConfirm(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String) {
        self.gotoConfirmTrasver?(viewController, id, name, phone, image, amount, notes)
    }
    
    
    public var gotoPinChek: ((_ viewController: UIViewController, _ id: Int, _ name: String, _ phone: String, _ image: String, _ amount: Int, _ notes: String, _ date: String, _ time: String) -> ())? = nil
    
    public func navigateToPinChek(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String) {
        self.gotoPinChek?(viewController, id, name, phone, image, amount, notes, date, time)
    }
    
    
    public var gotoFinalTransfer: ((_ viewController: UIViewController, _ id: Int, _ name: String, _ phone: String, _ image: String, _ amount: Int, _ notes: String, _ date: String, _ time: String, _ status: Bool) -> ())? = nil
    
    public func navigateToFinalTransfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String, status: Bool) {
        self.gotoFinalTransfer?(viewController, id, name, phone, image, amount, notes, date, time, status)
    }

    
    
    
    
//    id: Int, name: String, phone: String, image: String
    
}
