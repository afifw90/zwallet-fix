//
//  UserDefaultHelper.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation

public class UserDefaultHelper {
    
    public static var shared: UserDefaultHelper = UserDefaultHelper()
    
    var defaults = UserDefaults.standard
    
    public func set<T>(key: UserDefaultHelper.Key, value: T){
        self.defaults.set(value, forKey: key.rawValue)
    }
    
    public func get<T>(key: UserDefaultHelper.Key) -> T?{
        self.defaults.object(forKey: key.rawValue) as? T
    }
    
    public func remove(key: UserDefaultHelper.Key) {
        self.defaults.removeObject(forKey: key.rawValue)
    }
    
    
    public func set<T>(email: UserDefaultHelper.Email, value: T){
        self.defaults.set(value, forKey: email.rawValue)
    }
    
    public func get<T>(email: UserDefaultHelper.Email) -> T?{
        self.defaults.object(forKey: email.rawValue) as? T
    }
    
    public func remove(email: UserDefaultHelper.Email) {
        self.defaults.removeObject(forKey: email.rawValue)
    }
    
    
    public func set<T>(pin: UserDefaultHelper.Pin, value: T){
        self.defaults.set(value, forKey: pin.rawValue)
    }
    
    public func get<T>(pin: UserDefaultHelper.Pin) -> T?{
        self.defaults.object(forKey: pin.rawValue) as? T
    }
    
    public func remove(pin: UserDefaultHelper.Pin) {
        self.defaults.removeObject(forKey: pin.rawValue)
    }
}

public extension UserDefaultHelper {
    enum Key: String {
        case userToken
        case email
        case userTokenExpired
        case refreshToken
    }
    
    enum Email: String {
        case userEmail
    }
    
    
    enum Pin: String {
        case userPin
    }
}
