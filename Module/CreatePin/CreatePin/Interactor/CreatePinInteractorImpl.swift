//
//  CreatePinInteractorImpl.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core

class CreatePinInteractorImpl: CreatePinInteractor {
    
    var interactorOuput: CreatePinInteractorOuput?
    let authNetworkManager: AuthNetworkManager
    
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    
    
    func patchCreatePinData(pin: String) {
        self.authNetworkManager.setpin(pin: pin) { data, error in
            if data?.status==200 {
                self.interactorOuput?.authenticationResult(isSucces: true, message: data?.message ?? "")
            }else {
                self.interactorOuput?.authenticationResult(isSucces: false, message: data?.message ?? "")
            }
            
        }
    }
}
