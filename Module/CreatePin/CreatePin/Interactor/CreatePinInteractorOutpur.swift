//
//  CreatePinInteractorOutpur.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol CreatePinInteractorOuput {
    func authenticationResult(isSucces: Bool, message: String)
}
