//
//  CreatePinPresenterImpl.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation

class CreatePinPresenterImpl: CreatePinPresenter {
    
    
    let view: CreatePinView
    let interactor: CreatePinInteractor
    let router: CreatePinRouter
    
    init(view: CreatePinView, interactor: CreatePinInteractor, router: CreatePinRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func createPin(pin: String) {
        self.interactor.patchCreatePinData(pin: pin)
    }
}

extension CreatePinPresenterImpl: CreatePinInteractorOuput {
    func authenticationResult(isSucces: Bool, message: String) {
        if isSucces {
            
            
            self.router.navigateToHome()
            
        }else {
            self.view.showError(message: message)
        }
    }
    
}
