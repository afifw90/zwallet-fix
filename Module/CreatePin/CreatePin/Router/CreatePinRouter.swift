//
//  CreatePinRouter.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol CreatePinRouter {
    
    func navigateToHome()
    
}
