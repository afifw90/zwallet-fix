//
//  CreatePinRouterImpl.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core
import UIKit

public class CreatePinRouterImpl {
    
    public static func navigateToModule(){
        
        let bundle = Bundle(identifier: "com.mandiri.CreatePin")
        let vc = CreatePinViewController(nibName: "CreatePinViewController", bundle: bundle)
        
        let networkManager = AuthNetworkManagerImpl()
        
        let router = CreatePinRouterImpl()
        let interactor = CreatePinInteractorImpl(networkManager: networkManager)
        let presenter = CreatePinPresenterImpl(view: vc, interactor: interactor, router: router)
        
        
        interactor.interactorOuput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
}

extension CreatePinRouterImpl: CreatePinRouter {
    
    func navigateToHome() {
        AppRouter.shared.navigateToLogin()
    }
}
