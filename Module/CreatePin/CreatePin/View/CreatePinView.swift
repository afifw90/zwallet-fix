//
//  CreatePinView.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol CreatePinView {
    func showError(message: String)
}
