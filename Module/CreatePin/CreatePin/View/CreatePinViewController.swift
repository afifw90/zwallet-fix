//
//  CreatePinViewController.swift
//  CreatePin
//
//  Created by MacBook on 26/05/21.
//

import UIKit
import OTPFieldView

class CreatePinViewController: UIViewController {

    
    @IBOutlet weak var pinText: UITextField!
    
    
    @IBOutlet weak var succesViewPin: UIView!
    
    @IBOutlet weak var otpView: OTPFieldView!
    
    @IBOutlet weak var imageSucces: UIImageView!
    
    
    var pinSet: String = ""

    var presenter: CreatePinPresenter?
    
    @IBAction func btnSimpanPin(_ sender: Any) {
//        let pin: String = pinText.text ?? ""
        succesViewPin.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.presenter?.createPin(pin: self.pinSet)
        }
       
       
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
        succesViewPin.isHidden = true
        imageSucces.image = UIImage(named: "success", in: Bundle(identifier: "com.mandiri.CreatePin"), compatibleWith: nil)
        // Do any additional setup after loading the view.
    }

    func setupOtpView(){
        self.otpView.fieldsCount = 6
                self.otpView.fieldBorderWidth = 2
                self.otpView.defaultBorderColor = UIColor.black
                self.otpView.filledBorderColor = #colorLiteral(red: 0.3882352941, green: 0.4745098039, blue: 0.9568627451, alpha: 1)
                self.otpView.cursorColor = UIColor.red
                self.otpView.displayType = .roundedCorner
                self.otpView.fieldSize = 47
                self.otpView.separatorSpace = 10
                self.otpView.shouldAllowIntermediateEditing = false
                self.otpView.delegate = self
                self.otpView.initializeUI()
        }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreatePinViewController: CreatePinView {
    
    func showError(message: String) {
        let alert = UIAlertController(title: "Peringatan", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension CreatePinViewController: OTPFieldViewDelegate {
    
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
    print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        pinSet = otpString
        print("OTPString: \(otpString)")
    }
    
}
