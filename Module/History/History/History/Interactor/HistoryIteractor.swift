//
//  HistoryIteractor.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation


protocol HistoryIteractor {
    func getTransaction()
    func getAllTransaction()
}
