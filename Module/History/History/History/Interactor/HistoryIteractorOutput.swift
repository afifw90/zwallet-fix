//
//  HistoryIteractorOutput.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import Core

protocol HistoryIteractorOutput {
    func loadedTransaction(transactions: [TransactionEntity])
    func loadedAllTransaction(transactions: [TransactionEntity])
}
