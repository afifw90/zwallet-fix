//
//  HistoryPresenterImpl.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import UIKit
import Core

class HistoryPresenterImpl: HistoryPresenter {
    
    
    
    let view: HistoryView
    let iteractor: HistoryIteractor
    let router: HistoryRouter
    
    init(view: HistoryView, iteractor: HistoryIteractor, router: HistoryRouter) {
        self.view = view
        self.iteractor = iteractor
        self.router = router
    }
    
    func showHome() {
        self.router.navigateToMain()
    }
    
    func loadTransaction() {
        self.iteractor.getTransaction()
    }
    
    func loadAllTransaction() {
        self.iteractor.getAllTransaction()
    }
    
}

extension HistoryPresenterImpl: HistoryIteractorOutput {
    
    func loadedTransaction(transactions: [TransactionEntity]) {
        self.view.showTransactionData(transactions: transactions)
    }
    
    func loadedAllTransaction(transactions: [TransactionEntity]) {
        self.view.showTransactionData(transactions: transactions)
    }
    
}
