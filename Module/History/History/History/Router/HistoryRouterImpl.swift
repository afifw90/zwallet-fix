//
//  HistoryRouterImpl.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import UIKit
import Core

public class HistoryRouterImpl {
    
    public static func navigateToModule(){
        let bundle = Bundle(identifier: "com.mandiri.History")
        let vc = HistoryViewController(nibName: "HistoryViewController", bundle: bundle)
        
        let invoiceNetworkManager = InvoiceNetworkManagerImpl()
        
        let router = HistoryRouterImpl()
        let iteractor = HistoryIteractorImpl(invoiceNetworkManager: invoiceNetworkManager)
        let presenter = HistoryPresenterImpl(view: vc, iteractor: iteractor, router: router)
        
        
        iteractor.iteractorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
}

extension HistoryRouterImpl: HistoryRouter {
    
    func navigateToMain() {
        AppRouter.shared.navigateToHome()
    }
    
    
}
