//
//  HistoryViewController.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import UIKit
import Core

class HistoryViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = HistoryDataSource()
    
    var presenter: HistoryPresenter?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpTableView()
        self.presenter?.loadTransaction()
        self.presenter?.loadAllTransaction()
        // Do any additional setup after loading the view.
    }

    @IBAction func backTohome(_ sender: Any) {
        presenter?.showHome()
    }
    
    func setUpTableView(){
        self.dataSource.viewController = self
        
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "com.mandiri.Core")), forCellReuseIdentifier: "TransactionCell")
        
        self.tableView.dataSource = self.dataSource
    }
}
    
extension HistoryViewController: HistoryView {
    func showTransactionData(transactions: [TransactionEntity]) {
        self.dataSource.transactions = transactions
        self.tableView.reloadData()
    }
}
