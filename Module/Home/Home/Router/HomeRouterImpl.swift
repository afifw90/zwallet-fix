//
//  HomeRouterImpl.swift
//  Home
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import UIKit
import Core

public class HomeRouterImpl {
    
    public static func navigateToModul(){
        let bundle = Bundle(identifier: "com.mandiri.Home")
        let vc = HomeViewController(nibName: "HomeViewController", bundle: bundle)
        
        
        let balanceNetworkManager = BalanceNetworkManagerImpl()
        let invoiceNetwoerkManager = InvoiceNetworkManagerImpl()
        
        let router = HomeRouterImpl()
        let iteractor = HomeInteractorImpl(balanceNetworkManager: balanceNetworkManager, invoiceNetworkManager: invoiceNetwoerkManager)
        let presenter = HomePresenterImpl(view: vc, interactor: iteractor, router: router)
        
        iteractor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension HomeRouterImpl: HomeRouter {
    
    func navigateToHistory(viewController: UIViewController) {
        AppRouter.shared.navigateToHistory()
    }
    
    func navigateToLogin() {
        NotificationCenter.default.post(name: Notification.Name("reloadRootView"), object: nil)
    }
    
    func navigateToTransfer() {
        AppRouter.shared.navigateToContac()
    }
}
