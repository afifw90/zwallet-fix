//
//  DashboardCell.swift
//  Home
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Kingfisher

class DashboardCell: UITableViewCell {

    
    @IBOutlet weak var namaLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    var delegate: DashboardCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func showData(userProfile: UserProfileEntity) {
        
        self.namaLabel.text = userProfile.name
        self.balanceLabel.text = userProfile.balance.formatToIdr()
        self.phoneNumberLabel.text = userProfile.phoneNumber
        
        
        let url =  URL(string: userProfile.imageUrl)
        self.profileImage.kf.setImage(with: url)
        
    }
    
    
    @IBAction func showTransactionAction(_ sender: Any) {
        
        self.delegate?.showAllTransaction()
        
    }
    
    
    @IBAction func transferAction(_ sender: Any) {
        self.delegate?.showTransfer()
    }
    
    
    @IBAction func logoutAction(_ sender: Any) {
        
        self.delegate?.logout()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
