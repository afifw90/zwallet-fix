//
//  LoginInteractorImpl.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import Core


class LoginInteractorImpl: LoginInteractor {
    
    var interactorOutput: LoginInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager){
        self.authNetworkManager = networkManager
    }
    
    
    func postLoginData(email: String, password: String) {
        //hit api login dengan mengirimkan email dan password
        self.authNetworkManager.login(email: email, password: password) { data, error in
            if let loginData = data?.data {
                //menyimpan user token ke UserDefault
                UserDefaultHelper.shared.set(key: .userToken, value: loginData.token)
                UserDefaultHelper.shared.set(key: .email, value: loginData.email)
                UserDefaultHelper.shared.set(key: .refreshToken, value: loginData.refreshToken)
                //memberitahukan kepresenter jika proses berhasil
                
                let tokenExpiredDate: Date = Calendar.current.date(byAdding: .second, value: loginData.expiredIn / 1000, to: Date()) ?? Date()
                UserDefaultHelper.shared.set(key: .userTokenExpired, value: tokenExpiredDate)
                
                self.interactorOutput?.authenicationResult(isSuccess: true, status: data?.status ?? 0, hasPin: (data?.data.hasPin)!)
            } else {
                //memberitahukan kepresenter jika proses tidak berhasil
                self.interactorOutput?.authenicationResult(isSuccess: false, status: data?.status ?? 0, hasPin: false)
            
        }
    }
}
}
