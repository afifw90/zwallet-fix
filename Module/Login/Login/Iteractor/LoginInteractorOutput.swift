//
//  LoginInteractorOutput.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation


protocol LoginInteractorOutput {
    func authenicationResult(isSuccess: Bool, status: Int, hasPin: Bool)
}
