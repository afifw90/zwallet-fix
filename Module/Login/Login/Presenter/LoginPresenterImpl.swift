//
//  LoginPresenterImpl.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation

class LoginPresenterImpl: LoginPresenter {
    
    let view: LoginView
    let interector: LoginInteractor
    let router: LoginRouter
    
    init(view: LoginView, iteractor: LoginInteractor, router: LoginRouter) {
        
        self.view = view
        self.interector = iteractor
        self.router = router
    }
    
    func login(email: String, password: String) {
        self.interector.postLoginData(email: email, password: password)
    }
    
    func gotoSigup() {
        self.router.navigateToSignup()
    }
}

extension LoginPresenterImpl: LoginInteractorOutput {
    
    
    func authenicationResult(isSuccess: Bool, status: Int, hasPin: Bool) {
        if isSuccess {
            
            if status == 200 && hasPin == true {
                self.router.navigateToHome()
            }
            else if status == 200 && hasPin == false {
                self.router.navigateToCreatePin()
            }
        } else {
            self.view.showError()
        }
    }
    
}
