//
//  LoginRouter.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation

protocol LoginRouter {
    func navigateToHome()
    func navigateToSignup()
    func navigateToCreatePin()
}
