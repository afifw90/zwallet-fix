//
//  LoginRouterImpl.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import UIKit
import Core


public class LoginRouterImpl {
    
    public static func navigateToModule(){
        //digunakan utnuk menampilkan halaman login
        
        let bundle = Bundle(identifier: "com.mandiri.Login")
        let vc = LoginViewController(nibName: "LoginViewController", bundle: bundle)
        
        let networkManager = AuthNetworkManagerImpl()
        
        let router = LoginRouterImpl()
        let iterector = LoginInteractorImpl(networkManager: networkManager)
        let presenter =  LoginPresenterImpl(view: vc, iteractor: iterector, router: router)
        
        iterector.interactorOutput = presenter
        
        vc.presenter = presenter
        
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
}


extension LoginRouterImpl: LoginRouter {
    
    func navigateToHome() {
        NotificationCenter.default.post(name: Notification.Name("reloadRootView"), object: nil)
    }
    
    
    func navigateToSignup() {
        AppRouter.shared.navigateToRegister()
    }
    
    func navigateToCreatePin() {
        AppRouter.shared.navigateToCreatePin()
    }
}
