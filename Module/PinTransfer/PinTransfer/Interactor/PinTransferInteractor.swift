//
//  PinTransferInteractor.swift
//  PinTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol PinTransferInteractor {
    
    func postTransaction(receiver: Int, amount: Int, notes: String)

    
}
