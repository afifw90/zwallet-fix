//
//  PinTransferInteractorImpl.swift
//  PinTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core


class PinTransferInteractorImpl: PinTransferInteractor {
    
    var interactorOutput: PinTransferInteractorOutput?
    var contacNetWorkManager: ContacNetworkManager
    
    
    init(contacNetWorkManager: ContacNetworkManager) {
        
        self.contacNetWorkManager = contacNetWorkManager
        
    }
    
    func postTransaction(receiver: Int, amount: Int, notes: String) {
        
        
        self.contacNetWorkManager.createNewTransfer(receiver: receiver, amount: amount, notes: notes) { data, error in
            
            if let transferData = data {
                self.interactorOutput?.authenticationRersoult(isSuccess: true, status: data?.status ?? 0)
            } else {
                self.interactorOutput?.authenticationRersoult(isSuccess: false, status: data?.status ?? 0)
            }
            
        }
        
    }
    
}
