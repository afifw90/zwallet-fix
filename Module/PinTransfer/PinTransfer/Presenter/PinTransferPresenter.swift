//
//  PinTransferPresenter.swift
//  PinTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit

protocol PinTransferPresenter {
    func sendDataTransfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String, status: Bool)
    func hitApiTransfer(receiver: Int, amount: Int, notes: String)
    func bactoTransferConfirm(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String)
    
}
