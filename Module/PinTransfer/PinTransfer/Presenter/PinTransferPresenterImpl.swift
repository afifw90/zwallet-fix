//
//  PinTransferPresenterImpl.swift
//  PinTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

class PinTransferPresenterImpl: PinTransferPresenter {
    
    
    
    
    let view: PinTransferView
    let interactor: PinTransferInteractor
    let router: PinTransferRouter
    
    init(view: PinTransferView, interactor: PinTransferInteractor, router: PinTransferRouter) {
        
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func sendDataTransfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String, status: Bool) {
        router.navigateToSendDataPinChek(viewController: viewController, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes, date: date, time: time, status: status)
    }
    
    func hitApiTransfer(receiver: Int, amount: Int, notes: String) {
        self.interactor.postTransaction(receiver: receiver, amount: amount, notes: notes)
    }
    
    func bactoTransferConfirm(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String) {
        router.navigateToConfirm(viewController: viewController, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes)
    }
    
}


extension PinTransferPresenterImpl: PinTransferInteractorOutput {
   
    func authenticationRersoult(isSuccess: Bool, status: Int ) {
        print(status)
        if status == 200 {
            
                self.view.showError(pasing: isSuccess, status: status)
        }
        else {
            self.view.showError(pasing: isSuccess, status: status)
//            self.view.showWorngPin()
        }
    }
    
    
    
}


