//
//  PinTransferRouterImpl.swift
//  PinTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

public class PinTransferRouterImpl {
    
    public static func navigateToModule(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String){
        
        let bundle = Bundle(identifier: "com.mandiri.PinTransfer")
        let vc = PinTransferViewController(nibName: "PinTransferViewController", bundle: bundle)
        
        
        let networkManager = ContacNetworkManagerImpl()
        
        let router = PinTransferRouterImpl()
        let interactor = PinTransferInteractorImpl(contacNetWorkManager: networkManager)
        let presenter = PinTransferPresenterImpl(view: vc, interactor: interactor, router: router)
        
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        vc.id = id
        vc.name = name
        vc.phone = phone
        vc.image = image
        vc.amount = amount
        vc.notes = notes
        vc.date = date
        vc.time = time
        
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    
}


extension PinTransferRouterImpl: PinTransferRouter {
    
    func navigateToSendDataPinChek(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String, status: Bool) {
        AppRouter.shared.navigateToFinalTransfer(viewController: viewController, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes, date: date, time: time, status: status)
        
    }
    
   
    
    func navigateToConfirm(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String) {
        AppRouter.shared.navigateToTransferConfirm(viewController: viewController, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes)
    }
    
}
