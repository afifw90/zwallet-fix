//
//  PinTrasferRouter.swift
//  PinTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit

protocol PinTransferRouter {
    func navigateToSendDataPinChek(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String, status: Bool)
    
    
    func navigateToConfirm(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String)
    
}
