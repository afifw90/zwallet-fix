//
//  PinTransferView.swift
//  PinTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol PinTransferView {
    func showError(pasing: Bool, status: Int)
    func showWorngPin()
    
}
