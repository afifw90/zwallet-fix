//
//  PinTransferViewController.swift
//  PinTransfer
//
//  Created by MacBook on 29/05/21.
//

import UIKit
import OTPFieldView
import Core

class PinTransferViewController: UIViewController {

    
    
    @IBOutlet weak var otpView: OTPFieldView!
    
    var presenter: PinTransferPresenter?
    
    
    var id: Int = 0
    var name: String = ""
    var phone: String = ""
    var image: String = ""
    var amount: Int = 0
    var notes: String = ""
    var date: String = ""
    var time: String = ""
    var pasingSelec: Bool = false
    
    var statusTransfer: Int = 0
    
    
    var otpData: String = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
        
        //buat dibootom
        
        // Do any additional setup after loading the view.
    }

    @IBAction func backToConfirm(_ sender: Any) {
        presenter?.bactoTransferConfirm(viewController: self, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes)
        
        
    }
    
    @IBAction func goToFinalTransfer(_ sender: Any) {
        
        UserDefaultHelper.shared.set(pin: .userPin, value: otpData)
        presenter?.hitApiTransfer(receiver: id, amount: amount, notes: notes)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            print("\(self.statusTransfer)")
            if self.statusTransfer == 200 {
                self.presenter?.sendDataTransfer(viewController: self, id: self.id, name: self.name, phone: self.phone, image: self.image, amount: self.amount, notes: self.notes, date: self.date, time: self.time, status: self.pasingSelec)
            } else if self.statusTransfer == 500 {
                self.presenter?.sendDataTransfer(viewController: self, id: self.id, name: self.name, phone: self.phone, image: self.image, amount: self.amount, notes: self.notes, date: self.date, time: self.time, status: self.pasingSelec)
            }
            else {
                self.showWorngPin()
            }
        }
    }
    
    
    func setupOtpView(){
            self.otpView.fieldsCount = 6
            self.otpView.fieldBorderWidth = 2
            self.otpView.defaultBorderColor = UIColor.black
            self.otpView.filledBorderColor = UIColor.green
            self.otpView.cursorColor = UIColor.red
            self.otpView.displayType = .underlinedBottom
            self.otpView.fieldSize = 40
            self.otpView.separatorSpace = 8
            self.otpView.shouldAllowIntermediateEditing = false
            self.otpView.delegate = self
            self.otpView.initializeUI()
        }
    

}
extension PinTransferViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
            print("Has entered all OTP? \(hasEntered)")
            return false
        }
        
        func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
            return true
        }
        
        func enteredOTP(otp otpString: String) {
            otpData = otpString
            print("OTPString: \(otpString)")
        }
    
    
    
}


extension PinTransferViewController: PinTransferView {
    func showWorngPin() {
        let alert = UIAlertController (title: "Peringatan", message: "Pin Anda salah", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showError(pasing: Bool, status: Int) {
        if pasing {
            pasingSelec = true
            statusTransfer = status
        } else {
            pasingSelec = false
            statusTransfer = status
        }
    }
    

   
}
