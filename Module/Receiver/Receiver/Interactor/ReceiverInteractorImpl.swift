//
//  ReceiverInteractorImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core


class ReceiverInteractorImpl: ReceiverInteractor {
    
    
    var interactorOutput: ReceiverInteractorOutput?
    
    let contacNetworkManager: ContacNetworkManager
    
    init(contactNetworkManager: ContacNetworkManager) {
        self.contacNetworkManager = contactNetworkManager
    }
    
    func getUserContac() {
        self.contacNetworkManager.getAllContac { (data, error) in
            var contacs: [UserContacEntity] = []
            
            data?.forEach({ (contacData) in
                contacs.append(UserContacEntity(name: contacData.name, phone: contacData.phone, image: "\(AppConstant.baseUrl)\(contacData.image)", id: contacData.id))
                
            })
            
            self.interactorOutput?.loadedContacs(contancs: contacs)
        }
    }
    
}
