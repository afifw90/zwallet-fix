//
//  ReceiverPresenter.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit

protocol ReceiverPresenter {
    func loadContac()
    func backtoHome()
    func navigateToTransaction(viewController: UIViewController, id: Int, name: String, phone: String, image: String)
}
