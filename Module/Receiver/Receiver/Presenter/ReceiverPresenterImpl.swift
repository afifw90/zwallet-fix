//
//  ReceiverPresenterImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

class ReceiverPresenterImpl: ReceiverPresenter {
    
    
    
    
    
    let view: ReceiverView
    let interactor: ReceiverInteractor
    let router: ReceiverRouter
    
    init(view: ReceiverView, interactor: ReceiverInteractor, router: ReceiverRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadContac() {
        self.interactor.getUserContac()
    }
    
    func backtoHome() {
        self.router.backToHome()
    }
    
    func navigateToTransaction(viewController: UIViewController,id: Int, name: String, phone: String, image: String) {
        router.navigateToTransfer(viewController: viewController, id: id, name: name, phone: phone, image: image)
    }
    
}

extension ReceiverPresenterImpl: ReceiverInteractorOutput {
    
    func loadedContacs(contancs: [UserContacEntity]) {
        self.view.showContacData(contac: contancs)
    }
    
}
