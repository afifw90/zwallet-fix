//
//  ReceiverRouter.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit

protocol ReceiverRouter {
    
    func backToHome()
    func navigateToTransfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String)
}

