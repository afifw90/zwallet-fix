//
//  ReceiverRouterImpl.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class ReceiverRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.mandiri.Receiver")
        let vc = RecieverViewController(nibName: "RecieverViewController", bundle: bundle)
        
        let contacNetworkManeger = ContacNetworkManagerImpl()
        
        let router = ReceiverRouterImpl()
        let interactor = ReceiverInteractorImpl(contactNetworkManager: contacNetworkManeger)
        let presenter = ReceiverPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter 
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    
    
}

extension ReceiverRouterImpl: ReceiverRouter {
    
    func navigateToTransfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String) {
        AppRouter.shared.navigateToTransfer(viewController, id: id, name: name, phone: phone, image: image)
    }
    
    
    func backToHome() {
        AppRouter.shared.navigateToHome()
    }
    
    
}
