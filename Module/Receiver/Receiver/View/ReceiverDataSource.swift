//
//  ReceiverDataSource.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core
import UIKit

class ReceiverDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var viewController: RecieverViewController!
    
   
    var contacs: [UserContacEntity] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//       var countContacs: Int = contacs.count
        return contacs.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserContacTableViewCell") as! UserContacTableViewCell
        cell.showData(contacData: self.contacs[indexPath.row])
        cell.delegate = viewController.self
        
        return cell
    }
    
    
}
