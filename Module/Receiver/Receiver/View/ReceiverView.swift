//
//  ReceiverView.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

protocol ReceiverView {
    func showContacData(contac: [UserContacEntity])
}
