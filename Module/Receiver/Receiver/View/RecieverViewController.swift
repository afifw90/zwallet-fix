//
//  RecieverViewController.swift
//  Receiver
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Core


class RecieverViewController: UIViewController, UITableViewDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var countContacLabel: UILabel!
    
    
    var dataSource = ReceiverDataSource()
    
    var presenter: ReceiverPresenter?
    
    var contacNumber: Int?
    
    @IBAction func gotoHomeBtn(_ sender: Any) {
        presenter?.backtoHome()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setupTableView()
        self.presenter?.loadContac()
        // Do any additional setup after loading the view.
    }

    func setupTableView() {
        self.dataSource.viewController = self
        
        self.tableView.register(UINib(nibName: "UserContacTableViewCell", bundle: Bundle(identifier: "com.mandiri.Core")), forCellReuseIdentifier: "UserContacTableViewCell")
        
        self.tableView.dataSource = self.dataSource
        contacNumber = dataSource.contacs.count
        countContacLabel.text = "256 contact"
        self.tableView.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}


extension RecieverViewController: ReceiverView {
    
    func showContacData(contac: [UserContacEntity]) {
        self.dataSource.contacs = contac
        self.tableView.reloadData()
    }
    
}

extension RecieverViewController: UserContacDelegate {
    
    func showTransfer(id: Int, name: String, phone: String, image: String) {
        presenter?.navigateToTransaction(viewController: self, id: id, name: name, phone: phone, image: image)
    }
    
}
