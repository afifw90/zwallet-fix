//
//  RegisterIteractorImpl.swift
//  Register
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core

class RegisterIteractorImpl: RegisterIteractor {
    
    var interactorOutput: RegisterIteractorOutput?
    var authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    func postRegisterData(username: String, email: String, password: String) {
        
        self.authNetworkManager.signUp(username: username, email: email, password: password) { data, error in
            
            if data?.status==200 || data?.status==401 {
                
                self.interactorOutput?.authenticationResult(isSucces: true, message: data?.message ?? "" )
            } else {
                self.interactorOutput?.authenticationResult(isSucces: false, message: data?.message ?? "")
            }
            
        }
        
    }
}

