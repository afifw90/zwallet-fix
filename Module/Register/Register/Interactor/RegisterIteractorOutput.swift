//
//  RegisterIteractorOutput.swift
//  Register
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol RegisterIteractorOutput {
    func authenticationResult(isSucces: Bool, message: String)
}
