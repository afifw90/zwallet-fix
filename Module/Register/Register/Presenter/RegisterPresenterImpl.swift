//
//  RegisterPresenterImpl.swift
//  Register
//
//  Created by MacBook on 26/05/21.
//

import Foundation


class RegisterPresenterImpl: RegisterPresenter {
    
    let view: RegisterView
    let interactor: RegisterIteractor
    let router: RegisterRouter
    
    init(view: RegisterView, interactor: RegisterIteractor, router: RegisterRouter){
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func register(username: String, email: String, password: String) {
        self.interactor.postRegisterData(username: username, email: email, password: password)
    }
    
    func gotoLogin() {
        self.router.navigateToHome()
    }
}

extension RegisterPresenterImpl: RegisterIteractorOutput {
    func authenticationResult(isSucces: Bool, message: String) {
        if isSucces {
//            self.view.showError(message: message)
            self.router.navigateToCreatePin()
        }else {
            self.view.showError(message: message)
        }
    }
}
