//
//  RegisterRouter.swift
//  Register
//
//  Created by MacBook on 25/05/21.
//

import Foundation

protocol RegisterRouter {
    
    func navigateToCreatePin()
    func navigateToHome()
}
