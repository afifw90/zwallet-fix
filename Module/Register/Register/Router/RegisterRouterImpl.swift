//
//  RegisterRouterImpl.swift
//  Register
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import Core
import UIKit

public class RegisterRouterImpl {
    
    public static func navigateToModule(){
        let bundle = Bundle(identifier: "com.mandiri.Register")
        let vc = RegisterViewController(nibName: "RegisterViewController", bundle: bundle)
        
        
        let networkManager = AuthNetworkManagerImpl()
        
        let router = RegisterRouterImpl()
        let interactor = RegisterIteractorImpl(networkManager: networkManager)
        let presenter = RegisterPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
        
    }
    
}

extension RegisterRouterImpl: RegisterRouter {
    
    func navigateToCreatePin() {
        AppRouter.shared.navigateToSetOtp()
    }
    
    func navigateToHome() {
        AppRouter.shared.navigateToLogin()
    }
}
