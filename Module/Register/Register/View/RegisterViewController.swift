//
//  RegisterViewController.swift
//  Register
//
//  Created by MacBook on 25/05/21.
//

import UIKit
import Core


class RegisterViewController: UIViewController {

    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    
    var presenter: RegisterPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    
    @IBAction func goToLogin(_ sender: Any) {
        self.presenter?.gotoLogin()
    }
    
    @IBAction func buttonSigup(_ sender: Any) {
        
        let username: String = usernameText.text ?? ""
        let email: String = emailText.text ?? ""
        let password: String = passwordText.text ?? ""
        
        UserDefaultHelper.shared.set(email: .userEmail, value: email)
        
        self.presenter?.register(username: username, email: email, password: password)
        
    }
    

}

extension RegisterViewController: RegisterView {
    
    func showError(message: String) {
        let alert = UIAlertController(title: "Peringatan", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
