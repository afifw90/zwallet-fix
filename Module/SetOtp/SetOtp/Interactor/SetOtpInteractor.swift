//
//  SetOtpInteractor.swift
//  SetOtp
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol SetOtpInteractor {
    func getSetOtp(email: String, otp: String)
}
