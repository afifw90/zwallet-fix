//
//  SetOtpInteractorImpl.swift
//  SetOtp
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

class SetOtpInteractorImpl: SetOtpInteractor {
    
    var interactorOutput: SetOtpInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(netwoekManager: AuthNetworkManager) {
        
        self.authNetworkManager = netwoekManager
        
    }
    
    func getSetOtp(email: String, otp: String) {
        
        self.authNetworkManager.setOtp(email: email, otp: otp) { data, error in
            
            if data?.status == 200 {
                self.interactorOutput?.authenticationResult(isSucces: true, message: data?.message ?? "")
            } else {
                self.interactorOutput?.authenticationResult(isSucces: false, message: data?.message ?? "")
            }
            
        }
        
        
    }
    
    
}
