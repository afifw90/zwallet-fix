//
//  SetOtpInteractorOutput.swift
//  SetOtp
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol SetOtpInteractorOutput {
    
    func authenticationResult(isSucces: Bool, message: String)
    
}
