//
//  SetOtpPresenter.swift
//  SetOtp
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol SetOtpPresenter {
    func getOtp(email: String, otp: String)
}
