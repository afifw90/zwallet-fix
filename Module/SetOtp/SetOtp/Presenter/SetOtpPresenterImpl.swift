//
//  SetOtpPresenterImpl.swift
//  SetOtp
//
//  Created by MacBook on 27/05/21.
//

import Foundation

class SetOtpPresenterImpl: SetOtpPresenter {
    
    
    let view: SetOtpView
    let interactor: SetOtpInteractor
    let router: SetOtpRouter
    
    init(view: SetOtpView, interactor: SetOtpInteractor, router: SetOtpRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func getOtp(email: String, otp: String) {
        self.interactor.getSetOtp(email: email, otp: otp)
    }
    
}

extension SetOtpPresenterImpl: SetOtpInteractorOutput {
    
    func authenticationResult(isSucces: Bool, message: String) {
        if isSucces {
            
            self.router.navigateToCreatePin()
            
        }else {
            self.view.showError(message: message)
        }
    }
    
}
