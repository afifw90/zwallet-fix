//
//  SetOtpRouterImpl.swift
//  SetOtp
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class SetOtpRouterImpl {
    
    public static func navigateToModule() {
        
        let bundle = Bundle(identifier: "com.mandiri.SetOtp")
        let vc = SetOtpViewController(nibName: "SetOtpViewController", bundle: bundle)
        
        let networkManager = AuthNetworkManagerImpl()
        
        let router = SetOtpRouterImpl()
        let interactor = SetOtpInteractorImpl(netwoekManager: networkManager)
        let presenter = SetOtpPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    
}

extension SetOtpRouterImpl: SetOtpRouter {
    
    func navigateToCreatePin() {
        AppRouter.shared.navigateToLogin()
    }
    
}
