//
//  SetOtpView.swift
//  SetOtp
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol SetOtpView {
    func showError(message: String)
}
