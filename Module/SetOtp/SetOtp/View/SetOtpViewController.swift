//
//  SetOtpViewController.swift
//  SetOtp
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Core
import OTPFieldView

class SetOtpViewController: UIViewController {

    @IBOutlet weak var otpText: UITextField!
    
    @IBOutlet weak var otpField: OTPFieldView!
    
    var presenter: SetOtpPresenter?
    var otpSet: String = ""
    
    
    @IBAction func insertOtpBtn(_ sender: Any) {
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            let email: String? = UserDefaultHelper.shared.get(email: .userEmail)
            print(email!)
            self.presenter?.getOtp(email: email ?? "", otp: self.otpSet)
        }
        
        
        
    }
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
        // Do any additional setup after loading the view.
    }
    
    
    func setupOtpView(){
            self.otpField.fieldsCount = 6
            self.otpField.fieldBorderWidth = 2
            self.otpField.defaultBorderColor = UIColor.black
            self.otpField.filledBorderColor = UIColor.green
            self.otpField.cursorColor = UIColor.red
            self.otpField.displayType = .underlinedBottom
            self.otpField.fieldSize = 40
            self.otpField.separatorSpace = 8
            self.otpField.shouldAllowIntermediateEditing = false
            self.otpField.delegate = self
            self.otpField.initializeUI()
        }
}

extension SetOtpViewController: SetOtpView {
    
    func showError(message: String) {
        let alert = UIAlertController(title: "Peringatan", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SetOtpViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
           print("Has entered all OTP? \(hasEntered)")
           return false
       }
       
       func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
           return true
       }
       
       func enteredOTP(otp otpString: String) {
            otpSet = otpString
           print("OTPString: \(otpString)")
       }
}
