//
//  TransactionConfirmInteracorOutput.swift
//  TransactionConfirm
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

protocol TransactionConfirmInteractorOutput {
    func authenticationRersoult(isSuccess: Bool)
    func getBalance(balance: BalanceAmount)
}
