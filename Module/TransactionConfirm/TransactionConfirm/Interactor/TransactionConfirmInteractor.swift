//
//  TransactionConfirmInteractor.swift
//  TransactionConfirm
//
//  Created by MacBook on 28/05/21.
//

import Foundation


protocol TransactionConfirmInteractor {
    func postTransaction(receiver: Int, amount: Int, notes: String)
    func getBalance()
}
