//
//  TransactionConfirmInteractorImpl.swift
//  TransactionConfirm
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

class TransactionConfirmInterancorImpl: TransactionConfirmInteractor {
    
    
    
    var interactorOutput: TransactionConfirmInteractorOutput?
    var contacNetworkManager: ContacNetworkManager
    var balanceNetwork: BalanceNetworkManager
    
    init(networkManager: ContacNetworkManager, balanceNetwork: BalanceNetworkManager) {
        self.balanceNetwork = balanceNetwork
        self.contacNetworkManager = networkManager
    }
    
    
    func postTransaction(receiver: Int, amount: Int, notes: String) {
        self.contacNetworkManager.createNewTransfer(receiver: receiver, amount: amount, notes: notes) { data, error in
            
            if let transferData = data {
                self.interactorOutput?.authenticationRersoult(isSuccess: true)
            } else {
                self.interactorOutput?.authenticationRersoult(isSuccess: false)
            }
            
            
        }
    }
    
    func getBalance() {
        self.balanceNetwork.getBalance { (data, error) in
            if let balance = data {
                let amount = BalanceAmount(balance: balance.balance)
                self.interactorOutput?.getBalance(balance: amount)
            }
        }

    }
    
}
