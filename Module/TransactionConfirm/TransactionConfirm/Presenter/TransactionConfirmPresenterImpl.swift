//
//  TransactionConfirmPresenterImpl.swift
//  TransactionConfirm
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

class TransactionConfirmPresenterImpl: TransactionConfirmPresenter {
    
    
    
    
    
    let view: TransactionConfirmView
    let interactor: TransactionConfirmInteractor
    let router: TransactionConfirmRouter
    
    
    init(view: TransactionConfirmView, interactor: TransactionConfirmInteractor, router: TransactionConfirmRouter) {
        
        self.view = view
        self.interactor = interactor
        self.router = router
        
        
        
    }
    
    
    func transfer(viewController: UIViewController, id: Int, name: String, phone: String, image: String,  amount: Int, notes: String, date: String, time: String) {
        router.navigateToPinChek(viewController: viewController, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes, date: date, time: time)
    }
    
    func navigateToAmount(viewController: UIViewController, id: Int, name: String, phone: String, image: String) {
        
        router.navigateToAmount(viewController: viewController, id: id, name: name, phone: phone, image: image)
    }
    
    func getBalance() {
        self.interactor.getBalance()
    }
    
    
}

extension TransactionConfirmPresenterImpl: TransactionConfirmInteractorOutput {
    
    func getBalance(balance: BalanceAmount) {
        self.view.getBalance(balance: balance)
    }
    func authenticationRersoult(isSuccess: Bool) {
        
    }
}
