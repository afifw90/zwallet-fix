//
//  TransactionConfirmRouter.swift
//  TransactionConfirm
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit

protocol TransactionConfirmRouter {
    func navigateToPinChek(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String)
    func navigateToAmount(viewController: UIViewController, id: Int, name: String, phone: String, image: String)
    
    
}
