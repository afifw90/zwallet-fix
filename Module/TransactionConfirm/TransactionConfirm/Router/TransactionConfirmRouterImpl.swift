//
//  TransactionConfirmRouterImpl.swift
//  TransactionConfirm
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

public class TransactionConfirmRouterImpl {
    
    
    public static func navigateToModule(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String) {
        let bundle = Bundle(identifier: "com.mandiri.TransactionConfirm")
        let vc = TransactionConfirmViewController(nibName: "TransactionConfirmViewController", bundle: bundle)
        
        let contacNetworkManeger = ContacNetworkManagerImpl()
        let balanceNetworkManager = BalanceNetworkManagerImpl()
        
        let router = TransactionConfirmRouterImpl()
        let interactor = TransactionConfirmInterancorImpl(networkManager: contacNetworkManeger, balanceNetwork: balanceNetworkManager)
        let presenter = TransactionConfirmPresenterImpl(view: vc, interactor: interactor, router: router)
        
        
        interactor.interactorOutput = presenter
//        
        vc.prester = presenter
        vc.id = id
        vc.name = name
        vc.phone = phone
        vc.image = image
        vc.amount = amount
        vc.notes = notes
//
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
        
    }
    
    
    
    
}

extension TransactionConfirmRouterImpl: TransactionConfirmRouter {
    
    
    func navigateToPinChek(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String) {
        
        AppRouter.shared.navigateToPinChek(viewController: viewController, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes, date: date, time: time)
    }
    func navigateToAmount(viewController: UIViewController, id: Int, name: String, phone: String, image: String) {
        AppRouter.shared.navigateToTransfer(viewController, id: id, name: name, phone: phone, image: image)
    }
    
    
}
