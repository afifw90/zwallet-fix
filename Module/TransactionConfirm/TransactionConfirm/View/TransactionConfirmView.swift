//
//  TransactionConfirmView.swift
//  TransactionConfirm
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

protocol TransactionConfirmView {
    
    func showError()
    func getBalance(balance: BalanceAmount)
}
