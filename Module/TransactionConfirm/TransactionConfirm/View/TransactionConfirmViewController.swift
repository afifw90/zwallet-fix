//
//  TransactionConfirmViewController.swift
//  TransactionConfirm
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Kingfisher
import Core

class TransactionConfirmViewController: UIViewController {

   
    var id: Int = 0
    var name: String = ""
    var phone: String = ""
    var image: String = ""
    var amount: Int = 0
    var notes: String = ""
   
    
    var prester: TransactionConfirmPresenter?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var balaceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = name
        phoneNumber.text = phone
        profileImage.kf.setImage(with: URL(string: image))
        amountLabel.text = amount.formatToIdr()
        noteLabel.text = notes
     
        timeLabel.text = getCurrentTime()
        dateLabel.text = getCurrentDate()
        
        self.prester?.getBalance()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func gotoChekpin(_ sender: Any) {
       
        let getDate: String = getCurrentDate()
        let getTime: String = getCurrentTime()
        
        self.prester?.transfer(viewController: self, id: id, name: name, phone: phone, image: image, amount: amount, notes: notes, date: getDate, time: getTime )
//        print(dateText)
//        print(timeText)
    }
    
    
    
    @IBAction func gotoDetail(_ sender: Any) {
        
        self.prester?.navigateToAmount(viewController: self, id: id, name: name, phone: phone, image: image)
    }
    
    
    func getCurrentDate() -> String {
        
        let date = Date()
        let formater = DateFormatter()
        formater.locale = Locale(identifier: "ind")
        formater.dateFormat = "dd MM yyyy"
        
        let result = formater.string(for: date) ?? ""
        return result
        
    }

    func getCurrentTime() -> String {
        
        let date = Date()
        let formater = DateFormatter()
        formater.locale = Locale(identifier: "ind")
        formater.dateFormat = "HH:mm"
        
        let result = formater.string(for: date) ?? ""
        return result
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TransactionConfirmViewController: TransactionConfirmView {
    func getBalance(balance: BalanceAmount) {
        let resultBelance: Int = balance.balance - amount
        balaceLabel.text = resultBelance.formatToIdr()
    }
    
    
    func showError() {
        func showError() {
            let alert = UIAlertController (title: "Peringatan", message: "IsikanData", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
