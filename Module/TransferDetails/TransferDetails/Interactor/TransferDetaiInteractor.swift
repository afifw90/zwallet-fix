//
//  TransferDetaiInteractor.swift
//  TransferDetails
//
//  Created by MacBook on 30/05/21.
//

import Foundation

protocol TransferDetailInteractor {
    
    func getBalance()
    
}
