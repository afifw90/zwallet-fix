//
//  TransferDetailInteractorOutput.swift
//  TransferDetails
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Core


protocol TransferDetailInteractorOutput {
    
    func getBalance(balance: BalanceAmount)
}
