//
//  TransferDetailsInteractorImpl.swift
//  TransferDetails
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Core

class TransferDetailsInteractorImpl: TransferDetailInteractor{
    
    
    var interactorOutput: TransferDetailInteractorOutput?
    var balanceNetworkManager: BalanceNetworkManager
    
    
    init(balanceNetworkManager: BalanceNetworkManager) {
        self.balanceNetworkManager = balanceNetworkManager
    }
    
    func getBalance() {
        self.balanceNetworkManager.getBalance{ (data, error) in
            if let balance = data {
                let amount = BalanceAmount(balance: balance.balance)
                self.interactorOutput?.getBalance(balance: amount)
            }
            
        }
    }
    
}
