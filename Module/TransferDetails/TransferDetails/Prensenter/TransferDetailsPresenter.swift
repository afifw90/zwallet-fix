//
//  TransferDetailsPresenter.swift
//  TransferDetails
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol TransferDetailsPresenter {
    
    func navigateToHome()
    func getBalance()
}
