//
//  TransferDetailsPresenterImpl.swift
//  TransferDetails
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

class TransferDetailsPresenterImpl: TransferDetailsPresenter {
    
    
    
    let view: TransferDetailsView
    let router: TranferDeteilsRouter
    let interactor: TransferDetailInteractor
    
    init(view: TransferDetailsView, router: TranferDeteilsRouter, interactor: TransferDetailInteractor) {
        self.view = view
        self.router = router
        self.interactor = interactor
        
    }
    
    func navigateToHome() {
        router.navigateToHome()
    }
    
    
    func getBalance() {
        self.interactor.getBalance()
    }
}

extension TransferDetailsPresenterImpl: TransferDetailInteractorOutput {
    func getBalance(balance: BalanceAmount) {
        self.view.getBalance(balance: balance)
    }
    
    
    
}

