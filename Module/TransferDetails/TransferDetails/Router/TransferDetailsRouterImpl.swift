//
//  TransferDetailsRouterImpl.swift
//  TransferDetails
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

public class TransferDetailsRouterImpl {
    
    public static func navigateToModule(viewController: UIViewController, id: Int, name: String, phone: String, image: String, amount: Int, notes: String, date: String, time: String, status: Bool) {
        
        let  bundle = Bundle(identifier: "com.mandiri.TransferDetails")
        let vc = TransferDetailsViewController(nibName: "TransferDetailsViewController", bundle: bundle)
//        let contacNetworkManager = ContacNetworkManagerImpl()
        
        let balanceNetworkManager = BalanceNetworkManagerImpl()
        
        
        let router = TransferDetailsRouterImpl()
        let interactor = TransferDetailsInteractorImpl(balanceNetworkManager: balanceNetworkManager)
        let presenter = TransferDetailsPresenterImpl(view: vc, router: router, interactor: interactor)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        vc.id = id
        vc.name = name
        vc.phone = phone
        vc.image = image
        vc.amount = amount
        vc.notes = notes
        vc.date = date
        vc.time = time
        vc.status = status
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    

}

extension TransferDetailsRouterImpl: TranferDeteilsRouter {
    
    func navigateToHome() {
        AppRouter.shared.navigateToHome()
    }
    
    
}
