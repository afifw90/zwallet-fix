//
//  TransferDetailsView.swift
//  TransferDetails
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core

protocol TransferDetailsView {
    
    func showError()
    func getBalance(balance: BalanceAmount)
}
