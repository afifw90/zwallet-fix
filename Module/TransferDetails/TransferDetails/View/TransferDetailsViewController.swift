//
//  TransferDetailsViewController.swift
//  TransferDetails
//
//  Created by MacBook on 29/05/21.
//

import UIKit
import Kingfisher
import Core

class TransferDetailsViewController: UIViewController {

    
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var belanceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imageStatus: UIImageView!
    
    var id: Int = 0
    var name: String = ""
    var phone: String = ""
    var image: String = ""
    var amount: Int = 0
    var notes: String = ""
    var date: String = ""
    var time: String = ""
    var status: Bool = false
    
    var presenter: TransferDetailsPresenter?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = name
        amountLabel.text = amount.formatToIdr()
        dateLabel.text = "\(date)-\(time)"
        noteLabel.text = notes
        phoneLabel.text = phone
        
        imageProfile.kf.setImage(with: URL(string: image))
        
        if status == true {
            
            statusLabel.text = "Transaksi Berhasil"
            imageStatus.image = UIImage(named: "success", in: Bundle(identifier: "com.mandiri.TransferDetails"), compatibleWith: nil)
            
        } else {
            
            statusLabel.text = "Transaksi Gagal"
            imageStatus.image = UIImage(named: "failed", in: Bundle(identifier: "com.mandiri.TransferDetails"), compatibleWith: nil)
        }
        

        
        self.presenter?.getBalance()
        print("\(status)")
        
        
        
        
        // Do any additional setup after loading the view.
    }

    
    
    
    
    
    @IBAction func backToHome(_ sender: Any) {
        presenter?.navigateToHome()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TransferDetailsViewController: TransferDetailsView {
    func getBalance(balance: BalanceAmount) {
        belanceLabel.text = balance.balance.formatToIdr()
    }
    
    
    
    
    
    func showError() {
        let alert = UIAlertController (title: "Peringatan", message: "IsikanData", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}


