# Zwallet

Application e-wallet whit secure pin and otp.

## App Sample

![alt text](https://gitlab.com/afifw90/zwallet-fix/-/blob/main/sc.png)






## How to install

```python
unzip project, and open zwallet.xcworkspace
```

## Useg
This project for IoS 11.0 +

With viper design pattern VIPER : 
* View : for view controller your app
* Interactor: for buisnes logic your app
* Presenter: for way to connect view, router, interactor.
* Entity : for entity app
* Router : for navigate to another module
